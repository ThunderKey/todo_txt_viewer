# frozen_string_literal: true

require 'pathname'

require_relative 'todo_txt_viewer/version'
require_relative 'todo_txt_viewer/ui'

module TodoTxtViewer
  def self.ui
    folder = Pathname.new File.join(Dir.home, '.todo-txt')
    Ui.new folder.join('todo.txt'), folder.join('done.txt')
  end
end
