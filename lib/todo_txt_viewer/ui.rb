# frozen_string_literal: true

require 'todo-txt'
require 'fullscreen_tui'
require_relative 'task_line'
require_relative 'header_line'
require_relative 'help_ui'

module TodoTxtViewer
  class Ui < FullscreenTui
    def initialize main_file, archive_file
      super headers: []

      @main_file = main_file.to_s
      @archive_file = archive_file.to_s
      load_file
      self.selection += 1 if selected_line.is_a? HeaderLine
    end

    def key_press key # rubocop:disable Metrics/CyclomaticComplexity
      case key
      when 'd' then task_action :do!
      when 'u' then task_action :undo!
      when 'a' then archive_task
      when '+' then task_action :priority_inc!
      when '-' then task_action :priority_dec!
      when ('0'..'9') then select_priority key
      when 'i' then create_task
      when 'r' then load_file
      when 'h', '?' then help
      else super
      end
    end

    def go_up
      super
      return unless selected_line.is_a? HeaderLine

      selection.zero? ? go_down : super
    end

    def go_down
      super
      return unless selected_line.is_a? HeaderLine

      selection == lines.size - 1 ? go_up : super
    end

    private

    def task_action action
      return if selected_line.is_a? HeaderLine

      task = selected_line.task
      selected_line.task_action action
      @list.save!
      load_lines selected_task: task
    end

    def archive_task
      return if selected_line.is_a? HeaderLine

      archive = Todo::List.new @archive_file
      task = selected_line.task
      @list.delete_if {|other| other.object_id == task.object_id }
      archive << task
      archive.save!
      @list.save!
      load_lines
      footer.message = "Archived the Task #{task}"
    end

    def select_priority key
      return if selected_line.is_a? HeaderLine

      task = selected_line.task
      key = key.to_i
      priority = key.zero? ? nil : ('A'.ord + key - 1).chr
      task.instance_variable_set :@priority, priority
      @list.save!
      load_lines selected_task: task
    end

    def create_task
      line = read_full_line 'Please enter a task:'
      task = Todo::Task.new line
      task.instance_variable_set(:@created_on, Date.today) unless task.created_on
      @list << task
      @list.save!
      load_lines selected_task: task
      footer.message = 'Task inserted'
    end

    def help
      HelpUi.new.run
    end

    def load_file
      @list = Todo::List.new @main_file
      load_lines
      footer.message = "Loaded: #{@main_file}"
    end

    def load_lines selected_task: nil
      self.lines = []

      add_section_lines '# todo', @list.by_not_done
      add_section_lines '# done', @list.by_done

      return if selected_task.nil?

      idx = lines.index {|l| l.respond_to?(:task) && l.task.object_id == selected_task.object_id }
      self.selection = idx if idx
    end

    def add_section_lines title, data
      lines << HeaderLine.new(title)
      data.sort.reverse_each {|t| lines << TaskLine.new(t) }
    end
  end
end
