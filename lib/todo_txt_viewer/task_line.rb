# frozen_string_literal: true

module TodoTxtViewer
  class TaskLine
    ALLOWED_ACTIONS = %i(do! undo! priority_inc! priority_dec!).freeze

    attr_reader :task

    def initialize task
      @task = task
    end

    def to_s
      task.to_s
    end

    def task_action action
      raise "illegal action #{action}" unless ALLOWED_ACTIONS.include? action

      task.send action
    end
  end
end
