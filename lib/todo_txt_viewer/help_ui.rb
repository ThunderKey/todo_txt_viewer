# frozen_string_literal: true

module TodoTxtViewer
  class HelpUi < FullscreenTui
    CONTENT = {
      'd' => 'do/mark as done',
      'u' => 'undo/mark as todo',
      'a' => 'archive this task',
      '+' => 'increase the priority',
      '-' => 'decrease the priority',
      '1-9' => 'set priority to A-I',
      '0' => 'remove the priority',
      'i' => 'insert a new task',
      'r' => 'reload the file',
      'j/arrow down' => 'move down',
      'k/arrow up' => 'move up',
      'h/?' => 'show this help',
      'q' => 'quit',
    }.freeze

    def initialize
      super headers: ['Help']
      key_width = CONTENT.keys.map(&:size).max
      @lines = CONTENT.map {|k, v| "#{k.ljust key_width} = #{v}" }
    end
  end
end
