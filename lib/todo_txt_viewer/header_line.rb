# frozen_string_literal: true

module TodoTxtViewer
  class HeaderLine
    def initialize header
      @header = header.to_s
    end

    def to_s
      @header
    end

    def task_action(action) end
  end
end
