# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'todo_txt_viewer/version'

Gem::Specification.new do |spec|
  spec.name          = 'todo_txt_viewer'
  spec.version       = TodoTxtViewer::VERSION
  spec.authors       = ['Nicolas Ganz']
  spec.email         = ['nicolas@keltec.ch']

  spec.summary       = 'An interactive viewer for todo.txt'
  spec.homepage      = 'https://gitlab.com/ThunderKey/todo_txt_viewer'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'fullscreen_tui', '>= 0.1.2', '< 1'
  spec.add_dependency 'todo-txt', '~> 0.12'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
end
